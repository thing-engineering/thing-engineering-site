---
title: "The Great Network Rework"
date: 2019-10-17
draft: false
categories:
  - "Technology"
series: ["The Great Network Rework"]
tags:
  - "FreeNAS"
  - "Hardware"
  - "Networking"
---

My home network is a bit of a mess, this post documents my plans for a grand rework that will absolutely go exactly according to plan with no problems whatsoever :sweat_smile:

<!--more-->

## Current setup

* [Netgear CM1000](https://www.netgear.com/home/products/networking/cable-modems-routers/CM1000.aspx) cable modem for our Comcast "gigabit" internet
* [Netgear R7800 Nighthawk X4S](https://www.netgear.com/home/products/networking/wifi-routers/R7800.aspx) router/WAP running OpenWRT
* 8-port 1Gb switch for server and TV devices (TV, consoles, Raspberry Pi), connected to router
* 5-port 1Gb switch for computers (my desktop, wife's desktop, printer), connected to router with a too-short 50ft cable
* Server: [ASRock E3V5 WS](https://www.asrock.com/mb/Intel/E3V5%20WS/), [Intel Xeon E3 1230 V5](https://ark.intel.com/content/www/us/en/ark/products/88182/intel-xeon-processor-e3-1230-v5-8m-cache-3-40-ghz.html), 32GB DDR4, 500GB SSD, 2x 4TB HDD (mirror), 10TB HDD.
 
The existing server has to fill a lot of roles - NAS, development, home VMs, work VMs, and backup storage. All of the drives are full, there are no mirrors/backups for lots of data, and I find FreeBSD's hypervisor (bhyve) annoyingly wonky and difficult to use. 

---

## Plans!

This will take a fair while to finish due to :money_with_wings:

#### Separate NAS server

Breaking network file storage while messing around with VMs is not exactly a family-friendly feature, it's time to split the NAS functionality out to a new host.

* Buy a used enterprise rackmount machine. Currently leaning towards a system built around a [SuperMicro SC846](https://www.supermicro.com/en/products/chassis/4U/846/SC846A-R1200B) 24-bay 4U chassis.
* Add 2 small (60GB?) SSDs as a mirrored boot volume, I definitely don't trust the FreeNAS-recommended $10 USB stick option.
* Install [FreeNAS](https://www.freenas.org/), a FreeBSD-based NAS operating system. I'm tired of messing with the command line and would like things to mostly just work.
* Add a second-hand 2 port SFP+ network card for 10Gb, connected to VM host and switch.
* Move some (all?) of the existing drives over.

#### Storage

* Purchase 6x 8 or 10TB drives, possibly during Black Friday/Cyber Monday sales. Shucking external drives (removing them from their external USB cases) is usually [cheapest per TB](https://diskprices.com/?disk_types=external_hdd,internal_hdd&condition=new&units=TB).
* Set up an exciting RAID-Z2 array (survives 2 disks failing), roughly 32TB usable space with 8TB disks or 40TB with 10TB disks. This is fairly easy to expand later by adding more 6 disk arrays due to ZFS magic.

#### Re-purpose existing server

The hardware is only a few years old and still perfectly fine. The motherboard doesn't support CPUs with more than 4 cores/8 threads, if that becomes a bottleneck in the future I can probably pick up a used SuperMicro motherboard + dual E5 Xeon CPUs from Ebay relatively cheaply.

* Buy some sort of 4U chassis to re-home the existing server. A smaller case would also work since it doesn't have many drives, but cooling could be interesting. ATX power supply would be ideal!
  * Chenbro/iStarUSA/Norco are cheap and have middling reviews at best.
  * Rosewill (Newegg's brand I believe) are cheap, of wildly variable quality, and a huge pain to mount.
  * SuperMicro are high quality but also expensive, would also require a new PSU. 
* Add 2 small (60GB?) SSDs as a mirrored boot volume.
* Install [Proxmox VE](https://www.proxmox.com/en/proxmox-ve), a Linux-based virtualization platform for containers and virtual machines.
* Add a second-hand 1-2 port SFP+ network card for 10Gb, connected to NAS.
* Use the NAS for VM storage. iSCSI volumes? NFS?

#### Network

* Buy a rack-mount core switch! VLANs, 10Gb fiber, and various other fun things. Old enterprise gear can usually be found for cheap on Ebay, but they also tend to be SUPER LOUD. Some options:
  * [MikroTik CSS326-24G-2S+RM](https://mikrotik.com/product/CSS326-24G-2SplusRM) - 24x 1Gb, 2x 10Gb (SFP+) for $139
  * [MikroTik CRS328-24P-4S+RM](https://mikrotik.com/product/crs328_24p_4s_rm) - 24x 1Gb, 4x 10Gb (SFP+), PoE for $379. Power over Ethernet would provide future proofing for cameras/etc.
* Replace the current 50ft Cat6 cable from old router -> 5-port switch with a pair of 100ft cables so that it can actually reach the right part of the house - one for modem -> router, the other for core switch -> TV switch.
* Buy some sort of useful router device to run [pfSense](https://www.pfsense.org/) on. Many options here: [APU2](https://pcengines.ch/apu2.htm), [fitlet2](https://fit-iot.com/web/products/fitlet2/), etc.
* Move the existing router/WAP from the far end of the house to the new location, being closer to the middle should help with our signal woes.
  * If that doesn't work out, buy something new. The [Mikrotik Audience](https://mikrotik.com/product/audience) ($169) seems pretty cool, but there is also a wide range of [Ubiquiti UniFi wifi gear](https://www.ui.com/products/#unifi) that people seem to like.

#### Rack

Deciding on a rack to buy is proving quite difficult. An open 4 post rack is cheaper ($150+), but noise is possibly a problem. An enclosed cabinet rack is much more expensive ($400+), but dampens noise and conveniently has a top panel to sit things on.

* Raising Electronics make [15U 4-post 31" deep racks](https://risingracks.com/15u-4-post-open-frame-19-server-audio-rack-31-deep-1000mm/) ($136 pick-up but out of stock), unfortunately they don't seem to have a great reputation for quality.
* StarTech make [12U 4-post adjustable depth racks](https://www.amazon.com/StarTech-com-Open-Frame-Server-Rack/dp/B00P1RJ9LS/) ($203 shipped) that get much better reviews.
* Various companies make good cabinets but $$$.
* Picking up something second-hand on Craigslist/Ebay might work, but don't really have anywhere to put a huge full-height rack.