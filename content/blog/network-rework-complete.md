---
title: "Network Rework: Complete!"
date: 2019-12-12
draft: false
categories:
  - "Technology"
series: ["The Great Network Rework"]
tags:
  - "FreeNAS"
  - "Hardware"
  - "Networking"
  - "pfSense"
  - "Proxmox"
---

The network rework project is finally completed, many months later.

<!--more-->


## Hardware

### Server specs

- [SuperMicro SC846](https://www.supermicro.com/en/products/chassis/4U/846/SC846A-R1200B) 24-bay 4U chassis
- 1x [SuperMicro PWS-920P-SQ](https://store.supermicro.com/920w-1u-pws-920p-sq.html) power supply, second slot empty for now
- [SuperMicro X9DRi-F](https://www.supermicro.com/products/motherboard/Xeon/C600/X9DRi-F.cfm) motherboard
- 4x 16GB DDR3 memory
- 2x [Intel Xeon E5-2630L v2](https://ark.intel.com/content/www/us/en/ark/products/75791/intel-xeon-processor-e5-2630l-v2-15m-cache-2-40-ghz.html) CPUs (6 cores/12 threads)
- 2x [Noctua NH-D9DX i4 3U](https://noctua.at/en/products/cpu-cooler-workstation-server/nh-d9dx-i4-3u) CPU coolers
- 2x [Noctua NF A8 PWM](https://noctua.at/en/nf-a8-pwm) 80mm fans to replace exhaust fans
- 3x [Noctua NF-F12 iPPC 3000 PWM](https://noctua.at/en/nf-f12-industrialppc-3000-pwm) 120mm fans to replace fan wall
- [Solarflare SFN7122F](https://support.solarflare.com/index.php/component/cognidox/?view=categories&id=2706) dual-port SFP+ NIC, connected at 10Gbit via DAC cable
- LSI 9211-4i SAS2 HBA


### Storage

- 2x [Kingston A400 128GB](https://www.kingston.com/us/ssd/a400-solid-state-drive) SSDs as a mirrored boot/system volume, useless for anything else due to terrible performance. Turns out they're cheap for a reason!
- 2x [Samsung PM883 480GB](https://www.samsung.com/semiconductor/ssd/datacenter-ssd/MZ7LH480HAHQ/) SSDs as mirrored container/VM/homedir/scratch storage.
- 6x [Western Digital Easystore 12TB](https://www.bestbuy.com/site/wd-easystore-12tb-external-usb-3-0-hard-drive-black/6364259.p?skuId=6364259) external drives, removed from their cases ("shucked") and configured as a 42.2TiB ZFS RAIDZ2 pool.
- 1x [Seagate Ironwolf 10TB](https://www.seagate.com/internal-hard-drives/hdd/ironwolf/) + 1x [Western Digital Red 8TB](https://www.westerndigital.com/products/internal-drives/wd-red-hdd) HDDs in a janky 8TB mirror, currently unused and may just remove the drives entirely.

### Networking

- Router: [HP T730](https://www8.hp.com/us/en/thin-clients/desktop/t730.html) thin client + [Intel I350-T4V2](https://ark.intel.com/content/www/us/en/ark/products/84805/intel-ethernet-server-adapter-i350-t4v2.html) 4-port 1GbE NIC
- Switch: [Ruckus ICX 7150-C12P](https://www.ruckussecurity.com/ICX-7150-C12P.asp) - 14x 1GbE (12 with PoE/PoE+), 2x 1/10Gb SFP+ 

### "Rack"

Yup, it's some shelving on wheels. The chunky black case is the "old" server, currently turned off while I decide what on earth to do with all of the leftover hardware/components :sweat: Stitch is the official server mascot.

!["Rack"](/img/20191119_184810.jpg)


## Installation

### Router

Installed [pfSense](https://www.pfsense.org/) and spent a _LOT_ of time breaking the network, annoying my wife no end. Things are now in a working state, honest.

- All DNS requests are forced to go through the router, and the router uses DNS over HTTPS to avoid various ISP shenanigans
- pfBlockerNG blocks ads network-wide without having to install an ad blocker on every device. This breaks some garbage like the Amazon Android app, I'll eventually get around to whitelisting their ads.
- A permanent OpenVPN connection with a VLAN that can only exit the network via it. Not entirely sure what I want to use this for.

I ran into a lot of issues trying to use pfSense's DHCP/DNS with multiple subnets, the devs seem to be vehemently opposed to letting you do this for some reason.

### Server

After going through the usual "finding a USB stick that still works" adventure, I installed [FreeNAS](https://www.freenas.org/) and started trying to get a feel for it. It seems nice if you just want a NAS, or a NAS with a few extra bits, but I ultimately decided that it would make more sense to use the new monster server for everything. Compatibility is an issue (I write a lot of .NET Core lately), and FreeBSD's bhyve hypervisor is annoying at best.

Next adventure: [Proxmox VE](https://pve.proxmox.com/wiki/Main_Page). ZFS/NFS is managed directly on the server, and everything else runs in a large pile of lightweight Linux containers and occasional heavier VMs. The only real issue so far has been how easy it is to completely break the Proxmox web UI while trying to get container UID/GID mapping working.

It currently runs 9 containers and 2 VMs, with a lot more planned as I get deeper into [self-hosting things](https://github.com/awesome-selfhosted/awesome-selfhosted). The dnsmasq (multi-subnet DHCP/DNS) and Samba (Windows file sharing) containers are the only important ones, but hey, learning!

### Switch

Having never used enterprise networking gear before, [this ServeTheHome thread](https://forums.servethehome.com/index.php?threads/brocade-icx-series-cheap-powerful-10gbe-40gbe-switching.21107/) has been a _MASSIVE_ help. I now have VLANs, broadcast forwarding (DHCP!), and some very basic ACLs! VLANs:

- 10 `SECURE`: desktops (wired), laptops (wireless via secondary SSID). Can access everything in `SERVER`.
- 20 `INSECURE`: TVs/consoles/a whole lot of random garbage, mix of wired and wireless. Very limited access to `SERVER` services.
- 30 `VPN`: one toy container to make sure that it really does only work via the router's VPN.
- 40 `SERVER`: servers galore.

At some point I'll get around to having a separate VLAN for services (an internal DMZ?) to simplify ACLs, they're a bit horrifying right now.


## Future tasks

There's always more things to do, but learning new things is at least half of the fun for me!

#### Backups

Yeah, these need to happen. Super looking forward to the initial process of syncing the important stuff (cat/dog pictures and videos) to THE CLOUD with Comcast's miserly upstream.

#### Monitoring

Everyone likes pretty graphs, right? I'd like to be able to monitor a variety of metrics, and it looks like [Grafana](https://grafana.com/) (pretty dashboards) + [InfluxDB](https://www.influxdata.com/) (time series database) + [Telegraf](https://www.influxdata.com/time-series-platform/telegraf/) (metric collector) is the way to go.

- Temperatures, so many temperatures - drives/CPUs/etc across multiple machines
- CPU usage (Proxmox, all containers, all VMs)
- RAM usage (Proxmox, all containers, all VMs)
- Disk space (per pool? specific filesystems?)
- UPS power output/battery charge level/battery time remaining
- Server LAN bandwidth
- Internet bandwidth (from router)
- Latency to... somewhere?
- Switch stats? I think it has SNMP :thinking:
- Wifi stats?

#### Docker VMs

A large chunk of my work (and all of my weird side projects) use Docker/Docker Compose, but Docker for Windows is pretty bad. Probably makes sense to set up a Docker VM each for home/work. Will need to test to see how awful this will be via NFS.

#### Centralized user accounts

LDAP? FreeIPA? That other thing with a weird name? I'd like to not have to create user accounts over and over again.

#### Automate container/VM software updates

This is probably fairly easy to do, but it might as well make the list.

#### Network hardening

Probably need a proper DMZ setup for externally accessible services, even if that's just an SSH/VPN bastion host.

Separating the `SERVER` VLAN into `SERVICES` and `MANAGEMENT` might make sense too.
