---
title: "Buying Things"
date: 2019-10-27
draft: false
categories:
  - "Technology"
series: ["The Great Network Rework"]
tags:
  - "FreeNAS"
  - "Hardware"
  - "Networking"
---

Phase 1 of network upgrades: buying some things!

<!--more-->

I'm almost to the point where the NAS can actually be installed, configured, and tested with some existing drives.

## New NAS hardware

I ended up buying a used server with the following specs via Ebay:

- [SuperMicro SC846](https://www.supermicro.com/en/products/chassis/4U/846/SC846A-R1200B) 24-bay 4U chassis
- 2x [SuperMicro PWS-920P-1R](https://store.supermicro.com/920w-1u-pws-920p-1r.html) power supplies
- [SuperMicro X9DRi-F](https://www.supermicro.com/products/motherboard/Xeon/C600/X9DRi-F.cfm) motherboard
- 2x [Intel Xeon E5-2630L v2](https://ark.intel.com/content/www/us/en/ark/products/75791/intel-xeon-processor-e5-2630l-v2-15m-cache-2-40-ghz.html) 6 core/12 thread CPUs
- 4x 4GB ECC DDR3 RAM
- LSI 9211-4i SAS HBA

#### Project "Quieten the NAS"

Unsurprisingly, this machine is quite loud. A lot of research led me to some good ideas for quietening the machine while keeping it at acceptable temperatures, notably [this very useful YouTube video](https://www.youtube.com/watch?v=0UjyL6ZiMkI). I bought a few things so far:

- 1x [SuperMicro PWS-920P-SQ](https://store.supermicro.com/920w-1u-pws-920p-sq.html) power supply (**$50** used vs **$200** new!)
- 2x [Noctua NF A8 PWM](https://noctua.at/en/nf-a8-pwm) 80mm fans to replace the exhaust fans
- 3x [Noctua NF-F12 iPPC 3000 PWM](https://noctua.at/en/nf-f12-industrialppc-3000-pwm) 120mm fans to replace the mid-chassis fan wall
- 2x [Noctua NH-D9DX i4 3U](https://noctua.at/en/products/cpu-cooler-workstation-server/nh-d9dx-i4-3u) CPU coolers to replace the passive SuperMicro ones

Unfortunately, Amazon have somehow managed to get the 120mm fan order stuck in fulfillment limbo and they haven't even *SHIPPED* after a week :unamused:


#### Other NAS bits and pieces

- 1x [SuperMicro MCP-220-84603-0N](https://www.newegg.com/supermicro-mcp-220-84603-0n-drive-tray/p/N82E16816101828) dual 2.5" drive tray which attaches to the power supply wall
- 2x [Kingston A400 128GB](https://www.kingston.com/us/ssd/a400-solid-state-drive) SSDs for a mirrored boot volume ($22 each, madness)


## Network pieces

I finally decided on something to run [pfSense](https://www.pfsense.org/) or [OPNsense](https://opnsense.org/) for routing:

- [HP T730 thin client](https://www8.hp.com/us/en/thin-clients/desktop/t730.html) - powerful, quiet, and cheap ($140 shipped) 
- [Intel I350-T4V2 4-port 1GbE NIC](https://ark.intel.com/content/www/us/en/ark/products/84805/intel-ethernet-server-adapter-i350-t4v2.html) - 4 ports is overkill for now, but $39 with both brackets is hard to argue with. Hopefully it's a real one and not a [counterfeit](https://forums.servethehome.com/index.php?threads/comparison-intel-i350-t4-genuine-vs-fake.6917/)...


## A rack... sort of

Cheap racks tend to be low quality and difficult to ship, and good quality cabinets are a bit too expensive right now. Thanks to posts on [r/homelab](https://www.reddit.com/r/homelab/) I found a solution that will work for now, won't break the bank, and will be reusable in the future: [restaurant shelving](https://www.webstaurantstore.com/48403/regency-shelving.html) :sweat_smile:

- 4x [34" mobile shelving post](https://www.webstaurantstore.com/regency-34-nsf-green-epoxy-mobile-shelving-post/460EGPC34.html)
- 4x [21" x 30" shelves](https://www.webstaurantstore.com/regency-21-x-30-nsf-green-epoxy-wire-shelf/460EG2130.html)
- 1x [set of 4 casters](https://www.webstaurantstore.com/5-polyurethane-stem-shelving-casters-set/460EC26.html)

... for a total of **$115** shipped (inc. CA tax). Fits in the available space, strong enough to support heavy servers (much higher weight limits than cheap 4-post racks), and enough shelves to fit 2 servers + networking gear + printer + wifi + store some bits and pieces.


## Cables, exciting!

- 4x [24" SATA cables](https://www.monoprice.com/product?p_id=8779) - the only spare ones I have are 18" with awkward right-angle connectors
- 2x [75ft Cat6 ethernet cables](https://www.monoprice.com/product?p_id=9808) - 1 each of orange and purple because my wife liked those colors
- 2x [6ft power cables](https://www.monoprice.com/product?p_id=5279) - we somehow had none of these spare


## Rearranging things

- Removed the existing 50ft ethernet cable and ran the 2x 75ft ones. This was quite a workout, especially when I moved everything back and realized that I hadn't actually left enough slack to connect the cables :tired_face:
- Moved the router/WAP next to my computer where the "rack" will end up. Orange cable from modem to router, purple cable from router all the way back to the TV switch.
- Removed the 5 port switch that was connecting our desktops to the network, they're directly connected to the router for now.


## Next steps

Probably have to wait a while to buy anything else, _especially_ the drives. Hoping that external drives get solid discounts during Black Friday/Cyber Monday sales!

- Install the new CPU coolers in the NAS.
- Install FreeNAS on the NAS.
- Install pfSense or OPNsense on the new router machine.
- Work out cable management with the shelving.
- Move the UPS over to the shelving.
- Buy a small UPS for the modem, maybe?
- Buy a "new" network switch. Currently leaning towards a used [Brocade/Ruckus ICX 7150](https://www.ruckuswireless.com/products/campus-network-switches/ruckus-icx-family-switches/ruckus-icx-7150) 24- or 48-port PoE model if I can find one at a reasonable price. Very solid hardware, full L3 capabilities, and a convenient silent mode to run without fans until we need more than 150W of PoE.
- Buy some more RAM for the NAS, ideally taking it to 64GB.
- Buy 3x 10GbE NICs - 2 for servers, 1 for my workstation. 
- Buy drives :money_with_wings:
